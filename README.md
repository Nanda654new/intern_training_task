# intern_training_task



The task is to update subjects, batches, students, and, teachers' data from excel files into an SQL database and create a timetable based on the  slots selected by the students from the slots available in the teacher availability excel file.

## set up to run this project

To run this project you need to have SQL and python installed on your device. I did this using the PostgreSQL shell(https://www.postgresql.org/download/) and Visual Studio Code(python installed).

Download all the excel and python files from this repository.

Create SQL database and change the database crededentials in all the python files.

## steps to run this project

- first set up the device by following the above steps.
- fill the excel files with the available data.
- run the create_sql_tables python file 
it will create the following tables in the SQL database:
  - batches table with columns batch_id and batch_name with batch id as the primary key.
  - subjects table with columns subject_id and subject_name with subject_is as the primary key.
  - students table with columns student_id, student_name, and batch_id with student_id as the primary key and batch_id as the FK to the batches table.
  - teachers table with columns teachers_id, teachers_name, and subject_id with teachers_id as the primary key and subject_id as the FK to the subjects table.
  - teacher_availability table with columns teacher_id, tracher_name, day, and time.
  - time_table table with columns subject_name, day, and time.
- run the updating_values_in_sql_tables python file 
 - it will update all the data that is available in the corresponding tables in the SQL database.
![batches](images/batches.png)
![subjects](images/subjects.png)
![students](images/students.png)
![teachers](images/teachers.png)

- run the updating_teacher_availability_in_sql python file.
  - it will update the teacher availability data into the corresponding table in the SQL database
add image
![teacher_availability](images/teacher_availability.png)

- run the slot_selection python file and follow the procedure shown after running the file
 - it will first ask for the subject
   - if you enter the correct and available subject name it will then proceed to day selection.
   - else it will get two choices either to reselect thge subject or to exit the program.
 - after entering the prefered subject it will then ask to select the prefered day from the days shown.
   - if you enter the correct and available day then it will go for the time selection.
   - else it will get two choics either to reselect the day or to exit the program.
 - after entering the prefered day it will ask to select the prefered time from the shown available times.
   - if you enter the correct and available time it will ask for yor conformation to book the selected slot
   - else it will get two choices either to reselect the time or to exit the program.  
   
 