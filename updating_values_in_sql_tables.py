import openpyxl 
import psycopg2 

path_1 = ("subjects.xlsx") 
path_2 = ("batches.xlsx") 
path_3 = ("students.xlsx") 
path_4 = ("teachers.xlsx")
path_5 = ("teacher_availability.xlsx")
obj_sub = openpyxl.load_workbook(path_1)
obj_bat = openpyxl.load_workbook(path_2)
obj_stu = openpyxl.load_workbook(path_3)
obj_tec = openpyxl.load_workbook(path_4)
obj_tec_avai = openpyxl.load_workbook(path_5)
sheet_sub = obj_sub.active
sheet_bat = obj_bat.active 
sheet_stu = obj_stu.active
sheet_tec = obj_tec.active 
sheet_tec_avai = obj_tec_avai.active 
#print((sheet.cell(row = 1, column = 2)).value)
#print(sheet_sub.max_row)

conn = psycopg2.connect(
   database="intern_work",
    user='postgres',
    password='225866',
    host='localhost',
    port= '5432'
)
cursor = conn.cursor()

sql="insert into subjects(subject_id,subject_name)values(7,'new_4');"
#cursor.execute(sql)



sql_1 ="insert into subjects (subject_id,subject_name) values (%s,%s);"
sql_2 ="insert into batches (batch_id,batch_name) values (%s,%s);"
sql_3 ="update subjects set subject_name = %s where subject_id = %s;"
sql_4 ="update batches set batch_name = %s where batch_id = %s;"
sql_5 ="insert into students (student_id,student_name,batch_id) values (%s,%s,%s);"
sql_6 ="insert into teachers (teacher_id,teacher_name,subject_id) values (%s,%s,%s);"
sql_7= "select batch_id from batches where batch_name = '%s';"
sql_8= "select subject_id from subjects where subject_name = '%s';"


## Day 4 task

sub_r = sheet_sub.max_row
sub_c = sheet_sub.max_column
bat_r = sheet_bat.max_row
bat_c = sheet_bat.max_column
stu_r = sheet_stu.max_row
stu_c = sheet_stu.max_column
tec_r = sheet_tec.max_row
tec_c = sheet_tec.max_column

for i in range(sub_r):
    try:
        subject_id = (sheet_sub.cell(row = i+2, column = 1)).value
        subject_name = (sheet_sub.cell(row = i+2, column = 2)).value
        cursor.execute(sql_1,(subject_id,subject_name))
        conn.commit()
    except: 
        conn.commit()

for i in range(bat_r):
    try:
        batch_id = (sheet_bat.cell(row = i+2, column = 1)).value
        batch_name = (sheet_bat.cell(row = i+2, column = 2)).value
        cursor.execute(sql_2,(batch_id,batch_name))
        conn.commit()
    except:
        conn.commit()

sql_5 ="insert into students (student_id,student_name,batch_id) values (%s,%s,%s);"
sql_6 ="insert into teachers (teacher_id,teacher_name,subject_id) values (%s,%s,%s);"


## day 5 task
sql_7= "select batch_id from batches where batch_name = '%s';"
sql_8= "select subject_id from subjects where subject_name = '%s';"
for i in range(stu_r):
    try:
        student_id = (sheet_stu.cell(row = i+2, column = 1)).value
        student_name = (sheet_stu.cell(row = i+2, column = 2)).value
        batch_name = (sheet_stu.cell(row = i+2, column = 3)).value
        cursor.execute(sql_7%(batch_name))
        batch_id = cursor.fetchone()[0]
        #print(batch_id)
        conn.commit()
        cursor.execute(sql_5,(student_id,student_name,batch_id))
        conn.commit()
        #print(student_id)
    except: 
        conn.commit()
    
for i in range(tec_r):
    try:
        teacher_id = (sheet_tec.cell(row = i+2, column = 1)).value
        teacher_name = (sheet_tec.cell(row = i+2, column = 2)).value
        subject_name = (sheet_tec.cell(row = i+2, column = 3)).value
        cursor.execute(sql_8%(subject_name))
        subject_id = cursor.fetchone()[0]
        cursor.execute(sql_6,(teacher_id,teacher_name,subject_id))
        conn.commit()
        #print(teacher_id)
    except:
        conn.commit()  

## day 8-10 : Write code to extract availability data, perform string manipulation to convert it into a meaningful psql data and store it in the respective table

tec_avai_r = sheet_tec_avai.max_row
tec_avai_c = sheet_tec_avai.max_column

days = {"MONDAY":2,"TUESDAY":3,"WEDNESDAY":4,"THURSDAY":5,"FRIDAY":6,"SATURDAY":7}
sql_9= "select teacher_id from teachers where teacher_name = '%s';"
sql_10 ="insert into teacher_availability (teacher_id,teacher_name,day,time) values (%s,%s,%s,%s);"
for k in range(tec_avai_r):
    for i,j in days.items():
        try:
            teacher_name = (sheet_tec_avai.cell(row = k+1, column = 1)).value
            time = (sheet_tec_avai.cell(row = k+1, column = j)).value
            day = i
            #print(day)
            cursor.execute(sql_9%(teacher_name))
            teacher_id = cursor.fetchone()[0]
            z= []
            #print(teacher_id)
            x = time.split(",")
            #print(x)
            for i in x:
                #print(i)
                y = i.split("-")
                #print(y)
                for j in range(int(y[0]),int(y[1])+1):
                    #print(j)
                    z.append(j)
            #print(z)
            for i in z:
                time = str(i)+'-'+str(i+1)
                #print(time)
                cursor.execute(sql_10,(teacher_id,teacher_name,day,time))
                conn.commit()
        except:
            conn.commit()