import psycopg2

conn = psycopg2.connect(
   database="intern_work",
    user='postgres',
    password='225866',
    host='localhost',
    port= '5432'
)
cursor = conn.cursor()

sql_1= 'CREATE TABLE batches( batch_id INT, batch_name VARCHAR(50) UNIQUE NOT NULL, PRIMARY KEY(batch_id));'
sql_2= 'CREATE TABLE subjects( subject_id INT, subject_name VARCHAR(50) UNIQUE NOT NULL, PRIMARY KEY(subject_id));'
sql_3= 'CREATE TABLE students( student_id INT, student_name VARCHAR(50), batch_id INT, PRIMARY KEY(student_id),CONSTRAINT fk_batch_id FOREIGN KEY(batch_id) REFERENCES batches(batch_id) );'
sql_4= 'CREATE TABLE teachers( teacher_id INT, teacher_name VARCHAR(50) UNIQUE NOT NULL, subject_id INT, PRIMARY KEY(teacher_id),CONSTRAINT fk_subject_id FOREIGN KEY(subject_id) REFERENCES subjects(subject_id));'
sql_5= 'CREATE TABLE teacher_availability( teacher_id INT, teacher_name VARCHAR(50), day VARCHAR(50), time VARCHAR(50));'
sql_6= 'CREATE TABLE time_table( subject_name INT, day VARCHAR(50), time VARCHAR(50));'

cursor.execute(sql_1)
cursor.execute(sql_2)
cursor.execute(sql_3)
cursor.execute(sql_4)
cursor.execute(sql_5)
cursor.execute(sql_6)
conn.commit()