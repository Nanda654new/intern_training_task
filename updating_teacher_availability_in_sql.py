## day 8-10 : Write code to extract availability data, perform string manipulation to convert it into a meaningful psql data and store it in the respective table
import openpyxl 
import psycopg2 

path_5 = ("teacher_availability.xlsx")
obj_tec_avai = openpyxl.load_workbook(path_5)
sheet_tec_avai = obj_tec_avai.active 

conn = psycopg2.connect(
   database="intern_work",
    user='postgres',
    password='225866',
    host='localhost',
    port= '5432'
)
cursor = conn.cursor()

tec_avai_r = sheet_tec_avai.max_row
tec_avai_c = sheet_tec_avai.max_column

days = {"MONDAY":2,"TUESDAY":3,"WEDNESDAY":4,"THURSDAY":5,"FRIDAY":6,"SATURDAY":7}
sql_9= "select teacher_id from teachers where teacher_name = '%s';"
sql_10 ="insert into teacher_availability (teacher_id,teacher_name,day,time) values (%s,%s,%s,%s);"
for k in range(tec_avai_r):
    for i,j in days.items():
        try:
            teacher_name = (sheet_tec_avai.cell(row = k+1, column = 1)).value
            time = (sheet_tec_avai.cell(row = k+1, column = j)).value
            day = i
            #print(day)
            cursor.execute(sql_9%(teacher_name))
            teacher_id = cursor.fetchone()[0]
            z= []
            #print(teacher_id)
            x = time.split(",")
            #print(x)
            for i in x:
                #print(i)
                y = i.split("-")
                #print(y)
                for j in range(int(y[0]),int(y[1])+1):
                    #print(j)
                    z.append(j)
            #print(z)
            for i in z:
                time = str(i)+'-'+str(i+1)
                #print(time)
                cursor.execute(sql_10,(teacher_id,teacher_name,day,time))
                conn.commit()
        except:
            conn.commit()

